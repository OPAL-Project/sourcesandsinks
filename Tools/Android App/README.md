### Android App 
The app collects and displays necessary information about your device. Download and install the app (SourceSink.apk) in your Android smartphone. The app is safe to use; it does not store and send any data over the internet.
