package com.example.sourcesinkapp;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class myip extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myip);
        //Load WebView with HTML from Assets folder
        WebView myWebView = (WebView) findViewById(R.id.webView1);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.loadUrl("file:///android_asset/myip.html");
    }
}
