package com.example.sourcesinkapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button buttonOne = findViewById(R.id.buttonOne);
        buttonOne.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent getDeviceInfo = new Intent(getApplicationContext(), DeviceInfo.class);
                startActivity(getDeviceInfo);
            }
        });

        Button buttonTwo = findViewById(R.id.buttonTwo);
        buttonTwo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent activityTrafficStats = new Intent(getApplicationContext(), trafficstatistics.class);
                startActivity(activityTrafficStats);
            }
        });

        Button buttonThree = findViewById(R.id.buttonThree);
        buttonThree.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent installedApps = new Intent(getApplicationContext(), Listofapps.class);
                startActivity(installedApps);
            }
        });

        Button buttonFour = findViewById(R.id.buttonFour);
        buttonFour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent gryoscopeSensor = new Intent(getApplicationContext(), Gyroscope.class);
                startActivity(gryoscopeSensor);
            }
        });

        Button buttonFive = findViewById(R.id.buttonFive);
        buttonFive.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent gryoscopeSensor = new Intent(getApplicationContext(), Accelerometer.class);
                startActivity(gryoscopeSensor);
            }
        });


        Button buttonSix = findViewById(R.id.buttonSix);
        buttonSix.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent myIp = new Intent(getApplicationContext(), myip.class);
                startActivity(myIp);
            }
        });
    }
}