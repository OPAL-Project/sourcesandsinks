package com.example.sourcesinkapp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.widget.TextView;

import java.util.Locale;


public class DeviceInfo extends Activity {

    TextView textWifiStatus, textManufacture, textModel, textDeviceName,
            textMobileNetworkStatus, textKernelVersion, textSerial,
            textOperatorName, textOSVersion, textBuildNumber,
            textScreenResolution, textBluetoothStatus, deviceLanguage, uniqueID, gsfID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_device_info);

        textWifiStatus = (TextView) findViewById(R.id.wifi_status);
        textManufacture = (TextView) findViewById(R.id.manufacture);
        textModel = (TextView) findViewById(R.id.model);
        textDeviceName = (TextView) findViewById(R.id.device_name);
        textMobileNetworkStatus = (TextView) findViewById(R.id.mobile_network_status);
        textKernelVersion = (TextView) findViewById(R.id.kernel);
        textSerial = (TextView) findViewById(R.id.serial);
        textOperatorName = (TextView) findViewById(R.id.operatorName);
        textOSVersion = (TextView) findViewById(R.id.version_os);
        textBuildNumber = (TextView) findViewById(R.id.build_num);
        textScreenResolution = (TextView) findViewById(R.id.screen_res);
        textBluetoothStatus = (TextView) findViewById(R.id.bluetooth_status);
        deviceLanguage = (TextView) findViewById(R.id.device_language);
        uniqueID = (TextView) findViewById(R.id.uniqueID);
        gsfID = (TextView) findViewById(R.id.gsfID);


        textDeviceName.setText("Device name: " + Build.PRODUCT);

        textModel.setText("Model number: " + Build.MODEL); // get model name

        textManufacture.setText("Device Manufacturer: " + Build.MANUFACTURER); // get device manufacture

        gsfID.setText("Google Services Framework : " + getGSFID(this)); // get device Serial

        textSerial.setText("Hardware serial: " + Build.SERIAL); // get device Serial

        TelephonyManager tManager = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        textOperatorName.setText("Carrier Name: " + tManager.getNetworkOperatorName());

        textOSVersion.setText("Adnroid OS: " + Build.VERSION.RELEASE); // get OS version

        deviceLanguage.setText("Device Language: " + Locale.getDefault().getDisplayLanguage().toString());// device language

        textKernelVersion.setText("Kernel version: " + System.getProperty("os.version")); // get kernel version

        textWifiStatus.setText("WiFi status: " + isWifiNetworkAvailable(this)); // check wifi connection

        textMobileNetworkStatus.setText("Check 3G: " + isMobileNetworkAvailable(this)); //check 3G connection

        textBluetoothStatus.setText("Bluetooth status: " + checkBluetoothConnection()); //check bluetooth status

        textScreenResolution.setText("Device resolution: " + getDeviceScreenResolution()); //get device resolution

        textBuildNumber.setText("Device Build Fingerprints: " + Build.FINGERPRINT); //get Device's Build Number

        uniqueID.setText("ANDROID_ID : " + Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID));
    }

    public String getDeviceScreenResolution() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = size.x; //device width
        int height = size.y; //device height

        return "" + width + " x " + height; //example "480 * 800"
    }


    public String isWifiNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return "Connected";
        } else return "Disconnected";
    }


    public String isMobileNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return "Connected";
        } else {
            return "Disconnected";
        }

    }

    private String checkBluetoothConnection() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            return "Not supported";
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                return "Disabled";
            } else {
                return "Enabled";
            }
        }
    }


    private static final Uri sUri = Uri.parse("content://com.google.android.gsf.gservices");

    public static String getGSFID(Context context) {
        try {
            Cursor query = context.getContentResolver().query(sUri, null, null, new String[]{"android_id"}, null);
            if (query == null) {
                return "Not found";
            }
            if (!query.moveToFirst() || query.getColumnCount() < 2) {
                query.close();
                return "Not found";
            }
            final String toHexString = Long.toHexString(Long.parseLong(query.getString(1)));
            query.close();
            return toHexString.toUpperCase().trim();
        } catch (SecurityException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
