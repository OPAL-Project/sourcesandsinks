name := "Source2Sink"

version := "0.1"

scalaVersion := "2.12.6"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

libraryDependencies ++= Seq(
  "de.opal-project" % "abstract-interpretation-framework_2.12" % "1.1.0-SNAPSHOT" withSources() withJavadoc(),
  "de.opal-project" % "architecture-validation_2.12" % "1.1.0-SNAPSHOT" withSources() withJavadoc(),
  "org.scalatest" %% "scalatest" % "3.0.4" % "test"
)