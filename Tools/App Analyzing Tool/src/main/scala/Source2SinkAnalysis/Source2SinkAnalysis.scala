package Source2SinkAnalysis

/**
  * Identifying Sinks/Sources in Android Apps exposing privacy related information
  *
  * @author Ashiquer Rahman Kagozi
  */

import java.io.{BufferedWriter, FileWriter}
import java.net.URL

import org.opalj.br.analyses.{AnalysisExecutor, BasicReport, OneStepAnalysis, Project}
import org.opalj.br.instructions.MethodInvocationInstruction
import play.api.libs.json.Json

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

object Source2SinkAnalysis extends AnalysisExecutor {

  //Read Source, Sink and Permission lists
  val sourceList = Source.fromFile( "SourceSinkList/Sources.txt" ).getLines
  val sinkList = Source.fromFile( "SourceSinkList/Sinks.txt" ).getLines
  val permissionList = Source.fromFile( "SourceSinkList/Categories.txt" ).getLines.toList.map( _.split( " " ) )
    .groupBy( _.head ).mapValues( _.map( _.tail.mkString( " " ).trim ) )

  // Return the apk file's Sources and Sinks that are matched with our Source & Sink data set.
  val allApkMethods = ArrayBuffer[String]()
  var uniqueMethods = Set[String]()

  val analysis: OneStepAnalysis[URL, BasicReport] {} = new OneStepAnalysis[URL, BasicReport] {
    override def doAnalyze(
                            theProject: Project[URL],
                            parameters: Seq[String],
                            isInterrupted: () => Boolean): BasicReport = {

      if (theProject.allProjectClassFiles.nonEmpty) {
        for (method <- theProject.allMethodsWithBody) {
          method.body.get.instructions.foreach {
            //Get the apk's method name, return type, parameter and class name.
            case ii: MethodInvocationInstruction => allApkMethods += (ii.declaringClass.toJava + " " + ii.methodDescriptor.toJava( ii.name )).toList.mkString( "" )
            case _ =>
          }
        }
      }

      // Remove the duplicate methods.
      uniqueMethods = allApkMethods.toSet

      // Get the Source and Sink list and write them in a json file.
      val result = new BufferedWriter( new FileWriter( "Output/SourceSink.json" ) )
      result.write( Json.prettyPrint( Json.toJson( Map( "Source List" -> sourceSinks( sourceList, theProject, "Source" ), "Sink list" -> sourceSinks( sinkList, theProject, "Sink" ) ) ) ) )
      result.close()

      println( "\n\nSource and Sink lists created inside the Output/ directory.\n\n" )
      BasicReport( "" )
    }
  }

  /**
    * Read the Source and Sink text file from the directory
    * Find the usage of source and sinks in the apk file.
    * Create source and sink's method invocation in a json file.
    */
  def sourceSinks(readSourceSinkFile: Iterator[String], projectURL: Project[URL], Message: String): Map[String, Map[String, List[String]]] = {
    var matchedValue = ArrayBuffer[String]()
    readSourceSinkFile foreach {
      i =>
        uniqueMethods foreach {
          j =>
            i match {
              case s if s.startsWith( j ) => matchedValue += s.stripPrefix( "" ).replace( "android.permission.", "" )
              case _ =>
            }
        }
    }

    //Create Map of Sources and Sink which are matched between the apk file and Sources.txt,Sinks.txt files
    val sourceSinks = matchedValue.toList.map( _.split( " " ) ).groupBy( _.last ).mapValues( _.map( _.take( 3 ).mkString( " " ).trim ) )


    println( "\n\nGenerate " + Message + " invocation instructions" )

    //create source or sinks method invocation instructions
    var findInvokeMethods = ArrayBuffer[String]()
    sourceSinks foreach {
      i =>
        i._2.foreach( x =>
          for (method <- projectURL.allMethodsWithBody) {
            method.body.get.instructions.foreach {
              case ii: MethodInvocationInstruction =>
                if ((ii.declaringClass.toJava + " " + ii.methodDescriptor.toJava( ii.name )) == x) {
                  findInvokeMethods += ii.name + "-->" + method
                }
              case _ =>
            }
          }
        )
    }

    //Convert them in Map and write in a .json file
    val invokedMethods = findInvokeMethods.map( _.split("""-->""" ) ).groupBy( _.head ).mapValues( _.map( _.tail.mkString( " " ).trim ) )

    val result = new BufferedWriter( new FileWriter( "Output/" + Message + "InvocationInstruction.json" ) )
    result.write( Json.prettyPrint( Json.toJson( Map( "Invoked " + Message + " Methods" -> invokedMethods ) ) ) )
    result.close()

    println( "\n\nGenerate " + Message + "s Lists" )

    categoyrMap( sourceSinks )
  }


  /**
    * Categories the sources and sinks based on the category.txt file
    */
  def categoyrMap(sourceSinkMap: Map[String, List[String]]): Map[String, Map[String, List[String]]] = {
    val result = permissionList.mapValues {
      valuesList =>
        valuesList.flatMap {
          valueKey => sourceSinkMap.get( valueKey ).map( values => valueKey -> values )
        }.toMap
    }
    result.filter { case (_, values) => values.nonEmpty }
  }
}