### Identify Privacy-based Sources and Sinks in the Android app.

- The analyzing tool needs the Java bytecode of the apk file.
	- [enjarify](https://github.com/Storyyeller/enjarify "enjarify") tool  converts Dalvik bytecode to equivalent Java bytecode.
- 	Run the tool by specifying the converted apk files in the IDE’s ‘Program arguments’ section *cp=/meda/users/myapk-enjarify.jar*

- 	‘Output’ directory contains the Sources and Sinks list and their invoked methods lists.